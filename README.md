wikla
=====

Behavior Tree in Clo*j*ure.

[![wikla](https://clojars.org/wikla/latest-version.svg)](https://clojars.org/wikla)

## What is this?

Data oriented, deterministic, functional (incl. purity) implementation
of Behavior Tree, in the *AI, robotics and control* take.

### Theory

Behavior Trees are a model of plan execution, based on combining finite sets
of tasks in a modular fashion. They form a tree-like graph, where leafs
are performing some actions and returning Success/Failure/Running, which
is then used by branches to perform flow control. Branches use the same
convention to communicate up, so you can stack them as high as you wish.
Act of executing the tree is called a tick.

Inspired by [Marzinotto et al. (2014)][1] and contains implementations of:

* Condition
* Action
* Sequence
* Selection
* Parallel
* Decorator
* Ticker (the root node)

Not sourced from the above paper:

* Robin

### Implementation

Protocols, records and constructors. Plus one loop.

Mayor Downer: You are expected to return `Node` and `stash` explicitly
from your nodes. There are functions for that, however.

Minor Downer: You need `Ticker` as root to actually execute the tree.

```clojure
(require '[wikla.core :as bt])

;; Leaf
;;
;; Create a simple condition that checks if stash is within given limit.

(def C (bt/condition

         ;; Additional Condition properties, stored within instance.
         {:hi 3 :lo 1}

         ;; Function that will be called during a tick
         (fn status-fn [a-cond stash]
           (if (<= (:lo a-cond) stash (:hi a-cond))
             ;; Must return Success or Failure
             (bt/success a-cond stash)
             (bt/failure a-cond stash)))))

;; Condition can be ticked directly.

(bt/tick C 0)  ;; => #wikla.core.Result{...}

(bt/tick C 2)  ;; => #wikla.core.Result{...}

;; Composite
;;
;; Create simple sequence node using previously constructed leaf.

(def S (bt/sequence [C]))

;; We are expecting the sequence to return our condition next:
(= (bt/part S) C) ;; => true

;; We can now tick the condition and give it back to the sequence.

(bt/join S (bt/tick C 0)) ;; => #wikla.core.Result{...}

(bt/join S (bt/tick C 2)) ;; => #wikla.core.Result{...}

;; Ticker
;;
;; And in fact Ticker does exactly that. So let's seed it with our sequence.

(def T (bt/ticker S))

;; And tick it. To see our tree executed.

(bt/tick T 0) ;; => #wikla.core.Result{...}

(bt/tick T 2) ;; => #wikla.core.Result{...}
```

## Goals

    ☑  Get easy, general stuff in and working
    ☑  Get it right, test and abuse to make sure
    ☑  Get obvious deficiencies out
    ☐  Get something done with it, polish the API while doing so
    ☑  This is crazy, but tree visualisation maybe

## Usage

Not yet proven to be fit for any purpose. Unstable API. Experimental code.

Available from Clojars, include in your `project.clj` like:

[![wikla](https://clojars.org/wikla/latest-version.svg)](https://clojars.org/wikla)

[Marginalia for wikla][2] are available for your viewing pleasure.

You can export your trees to DOT graphs and view them using software like
[Graphviz][3].

### Brief what-is-where

* `wikla.core`, home of Node and Status protocols, records and constructors
* `wikla.pre`, holds some pre-defined `Condition`s, `Action`s and `decorator`s.
* `wikla.ext`, place for Nodes not source from the paper.
* `wikla.dot`, functions consuming Nodes and giving back DOTs.

## Code example

```clojure
(require '[wikla [core :refer :all]
                 [pre :refer [c:failure c:success]]])

(-> (ticker {:name "Demo!"}
            (selection {:name "One shall Stand, one shall Fall."}
                       [(sequence {:name "Fails fast."}
                                  [c:failure c:success])
                        (selection {:name "Fails not."}
                                   [c:failure c:success])]))
    (tick {})
    success?)
;; => true
```

By the way these are `dot/tree->svg` of this ticker:

#### Before `tick`

![Tree before tick.](http://spottr.bitbucket.org/wikla/pics/s-tree-0.svg)

#### After `tick`

![Tree after tick.](http://spottr.bitbucket.org/wikla/pics/s-tree-1.svg)

See `examples/` for more.

## Changes

* **0.3.2** Fixed exception handling in `Ticker` with `ex-info`.
* **0.3.1** Added `dot` for tree visualization via dot from Graphviz.
* **0.3.0** Re-done `Status`es, added `Robin` and improved tests. Moved stuff.
* **0.2.0** Merged `Ticker` with nodes, moved predefined nodes to `precooked`.
* **0.1.2** Simplified `Ticker` and added marginalia.
* **0.1.1** Fixes in `Ticker` and README.
* **0.1.0** Initial prototype.

## NAQ

**Q:** What's with the name?

**A:** Wikla *[ˈvʲikla]* is Polish for dense thicket, also an archaism for willow.

**Q:** But... why?

**A:** Because easier than willow, which is wierzba *[ˈvʲjɛʒba]*.

**Q:** Why do you expect me to give `props-map` to things like `selector`?

**A:** Introspection, printing, etc? If you give node a `:name` it prints
nicer than without it.

## License

Copyright © 2014-2015 Sławek Gwizdowski

License under **MIT Public License**, see **LICENSE** file in repository.

[1]: http://www.csc.kth.se/~miccol/Michele_Colledanchise/Publications_files/2013_ICRA_mcko.pdf
[2]: http://spottr.bitbucket.org/wikla/
[3]: http://www.graphviz.org/
