(require '[wikla.core :as bt])


;; Leaf
;;
;; Create a simple condition that checks if stash is within given limit.

(def C (bt/condition
         ;; Additional Condition properties, stored within instance.
         {:hi 3 :lo 1}
         ;; Function that will be called during a tick
         (fn status-fn [a-cond stash]
           (if (<= (:lo a-cond) stash (:hi a-cond))
             ;; Must return Success or Failure
             (bt/success a-cond stash)
             (bt/failure a-cond stash)))))

;; Condition can be ticked directly.

(bt/tick C 0)  ;; => #wikla.core.Result{...}

(bt/tick C 2)  ;; => #wikla.core.Result{...}


;; Composite
;;
;; Create simple sequence node using previously constructed leaf.

(def S (bt/sequence [C]))

;; We are expecting the sequence to return our condition next:

(= (bt/part S) C) ;; => true

;; We can now tick the condition and give it back to the sequence.

(bt/join S (bt/tick C 0)) ;; => #wikla.core.Result{...}

(bt/join S (bt/tick C 2)) ;; => #wikla.core.Result{...}


;; Ticker
;;
;; And in fact Ticker does exactly that. So let's seed it with our sequence.

(def T (bt/ticker S))

;; And tick it. To see our tree executed.

(bt/tick T 0) ;; => #wikla.core.Result{...}

(bt/tick T 2) ;; => #wikla.core.Result{...}

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

