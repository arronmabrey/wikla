(require '[wikla [core :refer :all]
                 [pre :refer [c:failure c:success
                              a:failure a:success
                              d:inversion]]
                 [ext :refer [robin]]
                 [dot :as dot]])

(def t (ticker {:name "Bigger Demo!"}
               (parallel {:name "One shall Stand, one shall Fall."}
                         2 3
                         [(robin {:name "Round round round."}
                                 success
                                 [(action {:name "'Tell me what to do'-action."}
                                          (fn [a s]
                                            ((:result s) a s)))
                                  a:success
                                  (d:inversion a:failure)])
                          (sequence {:name "Fails fast."}
                                    [c:failure c:success])
                          (selection {:name "Fails not."}
                                     [c:failure c:success])])))

(def running-stash {:result running})

(def success-stash {:result success})

(dot/tree->svg t "xl-tree-0.svg")

(def ticked-once (tick t running-stash))

(dot/tree->svg (:node ticked-once) "xl-tree-1.svg")

(def ticked-twice (tick (:node ticked-once) success-stash))

(dot/tree->svg (:node ticked-twice) "xl-tree-2.svg")

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

