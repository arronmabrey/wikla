(require '[wikla [core :refer :all]
                 [pre :refer [c:failure c:success]]
                 [dot :as dot]])

(def t (ticker {:name "Demo!"}
               (selection {:name "One shall Stand, one shall Fall."}
                          [(sequence {:name "Fails fast."}
                                     [c:failure c:success])
                           (selection {:name "Fails not."}
                                      [c:failure c:success])])))

(dot/tree->svg t "s-tree-0.svg")

(def ticked (tick t {}))

(success? ticked) ;; => true

(dot/tree->svg (:node ticked) "s-tree-1.svg")

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

