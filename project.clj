(defproject wikla "0.3.2"
  :description "Behavior Tree in Clojure.

Data oriented, deterministic, functional (incl. purity) implementation
of Behavior Tree in the *AI, robotics and control* take."
  :url "http://bitbucket.org/spottr/wikla/"
  :license {:name "MIT Public License"
            :distribution :repo
            :comments "LICENSE file in project root directory."}
  :dependencies [[org.clojure/clojure "1.7.0"]]
  :profiles {:dev {:dependencies [[midje "1.8.2"]
                                  [criterium "0.4.3"]]}
             ; lein with-profile +1.5:+1.6:+1.7 test!
             :1.5 {:dependencies [[org.clojure/clojure "1.5.1"]]}
             :1.6 {:dependencies [[org.clojure/clojure "1.6.0"]]}
             :1.7 {:dependencies [[org.clojure/clojure "1.7.0"]]}
             :1.8 {:dependencies [[org.clojure/clojure "1.8.0-RC1"]]}
             :uberjar {:aot :all}}
  :plugins []
  ;:main wikla.core
  :repl-options {:timeout 120000
                 :init-ns wikla.core
                 :init (do (println "welcome to wikla!"))}
  :aliases {"test!" ["do" "clean," "midje"]
            "pack!" ["do" "clean," "compile," "uberjar"]
            "prep!" ["do" "test!," "marg"]
            "autotest" ["midje" ":autotest"]}
  ;:global-vars {*warn-on-reflection* true}
  :omit-source false
  :target-path "target/"
  :jar-name "wikla-light.jar"
  :uberjar-name "wikla.jar"
  :jvm-opts [;"-server"
             ;"-XX:+AggressiveOpts"
             ;"-XX:+PrintFlagsFinal"
             ;"-Xms128M" "-Xmx128M"
             ])

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

