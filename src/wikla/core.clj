(ns wikla.core
"Behavior Tree basic building blocks.

`wikla.core` tries to follow terminology used in
_Towards a Unified Behavior Trees Framework for Robot Control_
[[Marzinotto et al. (2014)]][1]

To allow for purity `Node`s are split into leaf and composite, also `Status`
is used to drive the state, called stash, around.

Leafs get `tick`ed:

    (tick a-leaf stash)
      →  #Status{:node a-leaf* :stash stash*}

Composites provide children for execution:

    (part a-composite) →  #Node{...}

And compute `Status` when their child comes back as a `Status`:

    (join a-composite #Status{:node a-child* :stash stash*})
      →  #Status{:node a-composite* :stash stash**}

You can therefore have pure BT action if you wish. Or impure. Having options
is always nice.

For completeness fourth status is introduced: Ready. It's a default flag
for all freshly created nodes and can be used to ask the `Ticker` for immediate
re-execution. Unlike Success, Failure or Running it will
not be reported outside of the `Ticker`.

[1]: http://www.csc.kth.se/~miccol/Michele_Colledanchise/Publications_files/2013_ICRA_mcko.pdf"
  (:refer-clojure :exclude [sequence]))

(declare return-status success failure running ready
         return-node condition action
         sequence selection parallel decorator ticker
         reset status? node?)

;; ## Protocols

;; `Status` checks are done with shortcut methods.
(defprotocol Status
  (has-node? [this] "Is :node part of the payload?")
  (has-stash? [this] "Is :stash part of the payload?")
  (success? [this] "Is this Success?")
  (failure? [this] "Is this Failure?")
  (running? [this] "Is this Running?")
  (decided? [this] "Is this Success, Failure or Running?")
  (ready? [this] "Is this Ready?")
  (done? [this] "Is this Success or Failure."))

;; A `Node`'s life.
;;
;; `(branch? Node) →  true | false`
;;
;; `(part Node) →  Node | nil`
;;
;; `(join Node Status) →  Status`
;;
;; `(tick Node anything) →  Status`
(defprotocol Node
  (branch? [this] "True if composite, false if leaf.")
  (part [this] "Composite: returns next child to execute, nil if none.")
  (join [this sub] "Composite: accepts Status, returns Status.")
  (tick [this stash] "Leaf: accepts Stash, returns Status."))

;; ## Data Structures

;; ### Statuses

(def SUCCESS ::Success)

(def FAILURE ::Failure)

(def RUNNING ::Running)

(def READY ::Ready)

(defrecord Result [node stash]
  Status
  (has-node? [this]
    (node? (:node this)))
  (has-stash? [this]
    (contains? this :stash))
  (success? [this]
    (= SUCCESS (:flag node)))
  (failure? [this]
    (= FAILURE (:flag node)))
  (running? [this]
    (= RUNNING (:flag node)))
  (ready? [this]
    (= READY (:flag node)))
  (decided? [this]
    (or (success? this) (failure? this) (running? this)))
  (done? [this]
    (or (success? this) (failure? this))))

(extend clojure.lang.Keyword
  Status
  {:has-node?  (constantly false)
   :has-stash? (constantly false)
   :success?   (partial = SUCCESS)
   :failure?   (partial = FAILURE)
   :running?   (partial = RUNNING)
   :ready?     (partial = READY)
   :decided?   (partial contains? #{SUCCESS FAILURE RUNNING})
   :done?      (partial contains? #{SUCCESS FAILURE})})

;; ### Leaf Nodes

;; `Condition` wraps a single function: `status-fn`. Expectations:
;;
;; `(status-fn a-condition stash) →  Success | Failure`
;;
;; Changes done to `Condition` instance and stash are not persisted.
(defrecord Condition [status-fn flag]
  Node
  (branch? [this]
    false)
  (tick [this stash]
    (when (done? flag)
      (throw (ex-info "Ticking a finalized Condition!"
                      {:condition this :stash stash})))
    (let [flag* (status-fn this stash)]
      (if (done? flag*)
        (return-status flag* this stash)
        (throw (ex-info "Condition returned boom-boom!" {:flag flag*}))))))

;; `Action` wraps a single function: `tick-fn`. Expectations:
;;
;; `(tick-fn a-action stash) →  Success | Failure | Running`
;;
;; Changes done to `Action` instance are dropped, while changes to stash
;; are persisted.
(defrecord Action [tick-fn flag]
  Node
  (branch? [this]
    false)
  (tick [this stash]
    (when (done? flag)
      (throw (ex-info "Ticking a finalized Action!"
                      {:action this :stash stash})))
    (let [flag* (tick-fn this stash)]
      (if (decided? flag*)
        (return-status flag* this (:stash flag*))
        (throw (ex-info "Ticked Action status undecided!" {:flag flag*}))))))

;; ### Composite Nodes

;; Runs `children` from left to right, `Status` decided in `join`:
;;
;; * Success only when all children reported Success
;; * Failure immediately when child reported Failure
;; * Running immediately when child reported Running

(defrecord Sequence [children current flag]
  Node
  (branch? [branch]
    true)
  (part [this]
    (when-not (done? flag)
      (nth children current)))
  (join [this sub]
    (when (done? flag)
      (throw (ex-info "Joining into a finalized Sequence!"
                      {:sequence this :sub sub})))
    (when-not (and (status? sub) (has-node? sub) (has-stash? sub))
      (throw (ex-info "Status with :node and :stash expected!"
                      {:sequence this :sub sub})))
    (when-not (decided? sub)
      (throw (ex-info "Can't join undecided sub in Sequence!"
                      {:sequence this :sub sub})))
    (let [{:keys [node stash]} sub
          this* (-> this
                    (assoc-in [:children current] (:node sub))
                    (update-in [:current] inc))]
      (cond
        (running? sub)
        (running (update-in this* [:current] dec) stash)
        (failure? sub)
        (failure this* stash)
        (success? sub)
        (if (= (:current this*) (count children))
          (success this* stash)
          (ready this* stash))))))

;; Runs `children` from left to right, `Status` decided in `join`:
;;
;; * Success immediately when child reported Success
;; * Failure only when all children reported Failure
;; * Running immediately when child reported Running

(defrecord Selection [children current flag]
  Node
  (branch? [this]
    true)
  (part [this]
    (when-not (done? flag)
      (nth children current)))
  (join [this sub]
    (when (done? flag)
      (throw (ex-info "Joining into a finalized Selection!"
                      {:selection this :sub sub})))
    (when-not (and (status? sub) (has-node? sub) (has-stash? sub))
      (throw (ex-info "Status with :node and :stash expected!"
                      {:sequence this :sub sub})))
    (when-not (decided? sub)
      (throw (ex-info "Can't join undecided sub in Selection!"
                      {:selection this :sub sub})))
    (let [{:keys [node stash]} sub
          this* (-> this
                    (assoc-in [:children current] (:node sub))
                    (update-in [:current] inc))]
      (cond
        (running? sub)
        (running (update-in this* [:current] dec) stash)
        (success? sub)
        (success this* stash)
        (failure? sub)
        (if (= (:current this*) (count children))
          (failure this* stash)
          (ready this* stash))))))

;; Runs `children` from left to right, `Status` decided in last `join`:
;;
;; * Success if enough children reported Success
;; * Failure if enough children reported Failure
;; * Running if none of the above.

(defrecord Parallel [success-th failure-th children current retry flag]
  Node
  (branch? [this]
    true)
  (part [this]
    (when-not (done? flag)
      (nth children current)))
  (join [this sub]
    (when (done? flag)
      (throw (ex-info "Joining into a finalized Parallel!"
                      {:parallel this :sub sub})))
    (when-not (and (status? sub) (has-node? sub) (has-stash? sub))
      (throw (ex-info "Status with :node and :stash expected!"
                      {:sequence this :sub sub})))
    (when-not (decided? sub)
      (throw (ex-info "Can't join undecided sub in Parallel!"
                      {:parallel this :sub sub})))
    (let [{:keys [node stash]} sub
          this* (assoc-in this [:children current] node)]
      (if (running? sub)
        (running this* stash)
        (let [this** (update-in this* [:current] inc)]
          (if-not (< (:current this**) (count children))
            (let [succs (filter (comp success? :flag) (:children this**))
                  fails (filter (comp failure? :flag) (:children this**))]
              (cond
                (>= (count succs) success-th)
                (success this** stash)
                (>= (count fails) failure-th)
                (failure this** stash)
                :else
                (-> (reset this**)
                    (assoc :retry (inc retry))
                    (running stash))))
            (ready this** stash)))))))

;; Hybrid node, defining both `tick` and `part`/`join` interface.
;;
;; It holds a single child and decides if it should be executed.
;;
;; First `tick` is executed and calls:
;;
;; `(cond-fn a-decorator stash) →  boolean`
;;
;; **Scenario 1:** `cond-fn` returns `true`, then child will be executed.
;; Decorator changes its type to composite and signals Ready. When child
;; `join`s, it lands in `children` and `join-fn` is called:
;;
;; `(join-fn a-decorator stash) →  Status`
;;
;; **Scenario 2:** `cond-fn` returns `false`, child is not used, instead
;; `tick-fn` is executed during the `tick`:
;;
;; `(tick-fn a-decorator stash) →  Status`

(defrecord Decorator [children cond-fn join-fn tick-fn branching? flag]
  Node
  (branch? [this]
    branching?)
  (part [this]
    (when (and (branch? this) (not (-> children first :flag done?)))
      (first children)))
  (join [this sub]
    (when (done? flag)
      (throw (ex-info "Joining into a finalized Decorator!"
                      {:decorator this :sub sub})))
    (when-not (part this)
      (throw (ex-info "Extra joins are bad!" {:decorator this :sub sub})))
    (when-not (decided? sub)
      (throw (ex-info "Can't join undecided sub in Decorator!"
                      {:parallel this :sub sub})))
    (let [{:keys [node stash]} sub]
      (join-fn (assoc this :children [node]) stash)))
  (tick [this stash]
    (when (done? flag)
      (throw (ex-info "Ticking a finalized Decorator!"
                      {:decorator this :stash stash})))
    (when (branch? this)
      (throw (ex-info "Can't tick branching Decorator!"
                      {:decorator this :stash stash})))
    (if (cond-fn this stash)
      (ready (assoc this :branching? true) stash)
      (tick-fn this stash))))

;; ### Tickers

;; Basic `Ticker` is a loop that executes its `tree` until top level node
;; returns Success, Failure or Running. These `Status`es are propagated
;; back to root node. Ready status is executed immediately.

(defrecord Ticker [tree flag]
  Node
  (branch? [this]
    false)
  (tick [this stash]
    (when (done? flag)
      (throw (ex-info "Reset Ticker before re-running!" {:ticker this})))
    (loop [status (ready tree stash)
           stack  []]
      (let [node*  (:node status)
            stash* (:stash status)]
        (cond
          (decided? status)
          (if (empty? stack)
            (return-status status (assoc this :tree node*) stash*)
            (recur (join (peek stack) status) (pop stack)))
          (branch? node*)
          (if-let [next* (part node*)]
            (recur (ready next* stash*) (conj stack node*))
            (throw (ex-info "Undecided branch!"
                            {:ticker this :status status})))
          :else
          (let [status* (try
                          (tick node* stash*)
                          (catch Exception ex
                            (throw (ex-info "Exception during tick!"
                                            {:ticker this
                                             :node   node*
                                             :stash  stash*}
                                            ex))))]
            (when-not (status? status*)
              (throw (ex-info "Ticked node returned non-Status!"
                              {:ticker this :status status*})))
            (recur status* stack)))))))

;; ## API

;; ### Status

(defn return-status
  "Returns flag with updated `node` and `stash`.

  Does two things:

  1. Propagates `:flag` of given `node`
  2. Creates a `Result` with `node` and `stash`"
  [flag node stash]
  {:pre [(status? flag) (node? node)]
   :post [status? (complement keyword?)]}
  (if (keyword? flag)
    (->Result (assoc node :flag flag) stash)
    (->Result (assoc node :flag (-> flag :node :flag)) stash)))

(defn success
  "Creates `Result` with Success flagged `Node`."
  [node stash]
  {:pre [(node? node)]}
  (return-status SUCCESS node stash))

(defn failure
  "Creates `Result` with Failure flagged `Node`."
  [node stash]
  {:pre [(node? node)]}
  (return-status FAILURE node stash))

(defn running
  "Creates `Result` with Running flagged `Node`."
  [node stash]
  {:pre [(node? node)]}
  (return-status RUNNING node stash))

(defn ready
  "Creates `Result` with Ready flagged `Node`."
  [node stash]
  {:pre [(node? node)]}
  (return-status READY node stash))

;; ### Node

(defn return-node
  "Returns a node with properties applied safely and `:initial` meta field
  propagated for future `reset` calls."
  [props-map a-node]
  {:pre [(node? a-node)]
   :post [node?]}
  (let [safe-props (into props-map a-node)
        new-node   (into a-node safe-props)]
    (with-meta new-node (assoc (meta a-node) :initial new-node))))

(defn condition
  "Accepts properties and a function. Properties will be merged with returned
  `Condition`, while `status-fn` will be executed during `tick`.

  Expectation: `(status-fn a-condition stash) →  Result`"
  ([props-map status-fn]
    {:pre [(fn? status-fn)]}
    (return-node props-map
                 (map->Condition {:status-fn status-fn
                                  :flag      READY})))
  ([status-fn]
    (condition {} status-fn)))

(defn action
  "Accepts properties and a function. Properties will be merged with
  returned `Action` instance.

  Expectation: `(tick-fn a-action stash) →  Result`"
  ([props-map tick-fn]
    {:pre [(fn? tick-fn)]}
    (return-node props-map
                 (map->Action {:tick-fn tick-fn
                               :flag    READY})))
  ([tick-fn]
    (action {} tick-fn)))

(defn sequence
  "Accepts properties for `Sequence` as `hash-map` and a sequence of at least
  one node."
  ([props-map nodes]
    {:pre [(pos? (count nodes)) (every? node? nodes)]}
    (return-node props-map
                 (map->Sequence {:children (vec nodes)
                                 :current  0
                                 :flag     READY})))
  ([nodes]
    (sequence {} nodes)))

(defn selection
  "Accepts properties for `Selection` as `hash-map` and a sequence of at least
  one node."
  ([props-map nodes]
    {:pre [(pos? (count nodes)) (every? node? nodes)]}
    (return-node props-map
                 (map->Selection {:children nodes
                                  :current  0
                                  :flag     READY})))
  ([nodes]
    (selection {} nodes)))

(defn parallel
  "Accepts properties for `Parallel` as a `hash-map`, `success-threshold`,
  `failure-threshold` and a sequence of at least one node."
  ([props-map success-threshold failure-threshold nodes]
    {:pre [(and (number? success-threshold) (pos? success-threshold))
           (and (number? failure-threshold) (pos? failure-threshold))
           (<= success-threshold (count nodes))
           (<= failure-threshold (count nodes))
           (pos? (count nodes))
           (every? node? nodes)]}
    (return-node props-map
                 (map->Parallel {:success-th success-threshold
                                 :failure-th failure-threshold
                                 :children   nodes
                                 :current    0
                                 :retry      0
                                 :flag       READY})))
  ([success-threshold failure-threshold nodes]
    (parallel {} success-threshold failure-threshold nodes)))

(defn decorator
  "Accepts `cond-fn`, `join-fn`, `tick-fn` and single node.

  Expectations:

    (cond-fn a-decorator stash) →  boolean
    (join-fn a-decorator stash) →  Status
    (tick-fn a-decorator stash) →  Status"
  ([props-map cond-fn join-fn tick-fn node]
    {:pre [(fn? cond-fn)
           (fn? join-fn)
           (fn? tick-fn)
           (node? node)]}
    (return-node props-map
                 (map->Decorator {:children   [node]
                                  :cond-fn    cond-fn
                                  :join-fn    join-fn
                                  :tick-fn    tick-fn
                                  :branching? false
                                  :flag       READY})))
  ([cond-fn join-fn tick-fn node]
    (decorator {} cond-fn join-fn tick-fn node)))

;; ### Ticker

(defn ticker
  "Accepts properties and a node."
  ([props-map tree]
    {:pre [(node? tree)]}
    (return-node props-map (map->Ticker {:tree tree :flag READY})))
  ([tree]
    (ticker {} tree)))

;; ### Helpers

(defn reset
  "Accepts a `node` and cycles it to fresh state."
  [node]
  {:pre [(node? node)]}
  (if-let [node* (-> node meta :initial)]
    (with-meta node* (meta node))
    (throw (ex-info "Initial Node not found!" {:node node}))))

(defn status?
  "Is something a `Status`?"
  [something]
  (satisfies? Status something))

(defn node?
  "Is something a `Node`?"
  [something]
  (satisfies? Node something))

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

