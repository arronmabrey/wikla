(ns wikla.dot
  "Consume Nodes, return DOTs."
  (:refer-clojure :exclude [sequence])
  (:require [wikla.core :refer :all]
            [wikla.ext :refer :all]
            [clojure.java.shell :refer [sh]])
  (:import [wikla.core Condition Action
                       Sequence Selection Parallel Decorator Ticker]
           [wikla.ext Robin]))

;; ## Protocol

(defprotocol Dot
  (shape [this] "Desired Shape.")
  (label [this] "Node Description.")
  (children [this] "Children to be graphed.")
  (color [this] "Color."))

;; ## Helpers!

(defn node->class
  "Eats Node, returns its class name as string, no namespace."
  [a-node]
  {:pre [(node? a-node)]}
  (->> (class a-node)
       str
       (re-find #".+\.([^\.]+)$")
       last))

(defn node->color
  "Eats Node, returns color depending on flag."
  [a-node]
  {:pre [(node? a-node)]}
  (let [{:keys [flag]} a-node]
    (cond
      (success? flag) "lime"
      (failure? flag) "crimson"
      (running? flag) "deepskyblue"
      :else "silver")))

(defn node->style
  "Eats Node, returns DOT style with shape, color and label."
  [a-node]
  {:pre [(node? a-node)]}
  (format "[shape=%s, color=%s, label=\"%s\"]"
          (shape a-node)
          (color a-node)
          (label a-node)))

;; ## Default Dot protocol implementation

(def defaults
  {:shape (constantly "rect")
   :label (fn [n] (if (:name n)
                    (str (node->class n) ": " (:name n))
                    (node->class n)))
   :children (fn [n] (:children n))
   :color node->color})

;; ## Node extensions

(extend wikla.core.Condition
  Dot
  (assoc defaults
         :shape (constantly "oval")
         :children (constantly nil)))

(extend wikla.core.Action
  Dot
  (assoc defaults
         :shape (constantly "rect")
         :children (constantly nil)))

(extend wikla.core.Sequence
  Dot
  defaults)

(extend wikla.core.Selection
  Dot
  defaults)

(extend wikla.core.Parallel
  Dot
  (assoc defaults
         :label (fn [n]
                  (let [flags (format " (s=%d, f=%d)"
                                      (:success-th n)
                                      (:failure-th n))]
                    (if (:name n)
                      (str (node->class n) flags ": " (:name n))
                      (str (node->class n) flags))))))

(extend wikla.core.Decorator
  Dot
  defaults)

(extend wikla.core.Ticker
  Dot
  (assoc defaults
         :shape (constantly "diamond")
         :children (fn [s] [(:tree s)])))

(extend wikla.ext.Robin
  Dot
  defaults)

;; ## Generate DOT string and dump to files.

(defn tree->str
  "Eats Node, returns DOT graph string."
  [tree]
  {:pre [(node? tree)]}
  (letfn [(nid [n]
            (str "node" n))
          (pairs [n cs]
            (when (seq cs) (mapv #(assoc % :p-node n) cs)))
          (dot [log]
            (format "graph \"a tree\" {\n%s}"
                    (apply str (interleave log (repeat \newline)))))]
    (loop [idx 0 log [] curr tree more []]
      (if curr
        (let [n (nid idx)
              p (:p-node curr)
              s (node->style curr)
              c (color curr)
              m (concat more (pairs n (children curr)))]
          (recur (inc idx)
                 (into log [(when p (format "%s -- %s [color=%s];" p n c))
                            (format "%s %s" n s)])
                 (first m)
                 (rest m)))
        (dot log)))))

(defn tree->dot
  "Eats Node, saves tree to DOT."
  [tree file-name]
  (io! "Because File Saving."
    (or (spit file-name (tree->str tree)) true)))

(defn tree->svg
  "Eats Node, saves tree to SVG. (Shells out to dot.)"
  [tree file-name]
  (io! "Shelling out to clojure.java.shell/sh"
    (-> (sh "dot" "-Tsvg" (str "-o" file-name) :in (tree->str tree))
        :exit
        zero?)))

(defn tree->png
  "Eats Node, saves tree to PNG. (Shells out to dot.)"
  [tree file-name]
  (io! "Shelling out to clojure.java.shell/sh"
    (-> (sh "dot" "-Tpng" (str "-o" file-name) :in (tree->str tree))
        :exit
        zero?)))

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

