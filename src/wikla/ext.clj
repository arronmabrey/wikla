(ns wikla.ext
  "More Nodes."
  (:refer-clojure :exclude [sequence])
  (:require [wikla.core :refer :all]))


;; ## Data structures

;; ### Composite Nodes

;; Runs children in a round-robin fashion, until all are done. Then, finally
;; calls the `summary-fn` with self and final stash. Child that returned
;; Running is not re-run immediately, nor does `Robin` immediately report
;; Running, instead next child is selected for execution. `Robin` only
;; reports Running when next child is already Running.

(defrecord Robin [children summary-fn current flag]
  Node
  (branch? [this]
    true)
  (part [this]
    (when-not (done? flag)
      (nth children current)))
  (join [this sub]
    (when (done? flag)
      (throw (ex-info "Joining into a finalized Robin!"
                      {:robin this :sub sub})))
    (when-not (and (status? sub) (has-node? sub) (has-stash? sub))
      (throw (ex-info "Status with :node and :stash expected!"
                      {:robin this :sub sub})))
    (when-not (decided? sub)
      (throw (ex-info "Can't join undecided sub in Robin!"
                      {:robin this :sub sub})))
    (let [{:keys [node stash]} sub
          this* (assoc-in this [:children current] node)
          steps (range (inc current) (+ (inc current) (count children)))
          indxr (fn [step]
                  (mod step (count children)))
          next? (fn [idx]
                  (-> this* :children (nth idx) :flag done? not))]
      (if-let [next-child (first (filter next? (map indxr steps)))]
        (if (-> this :children (nth next-child) :flag running?)
          (running (assoc this* :current next-child) stash)
          (ready (assoc this* :current next-child) stash))
        (summary-fn this* stash)))))

;; ## API

;; ### Nodes

(defn robin
  "Accepts properties for `Robin` as a `hash-map`, `summary-fn` called when
  all children are done and a sequence of at least one node."
  ([props-map summary-fn nodes]
    {:pre [(pos? (count nodes))
           (every? node? nodes)
           (fn? summary-fn)]}
    (return-node props-map
                 (map->Robin {:children   (vec nodes)
                              :summary-fn summary-fn
                              :current    0
                              :flag       READY})))
  ([summary-fn nodes]
    (robin {} summary-fn nodes)))


; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

