(ns wikla.pre
  "Example and pre-cooked nodes."
  (:require [wikla [core :refer [success failure running ready return-status
                                 success? failure? running? ready?
                                 condition action decorator]]]))


;; ## Dumb, predefined Conditions

(def c:success (condition {:name "SUCCESS"} success))

(def c:failure (condition {:name "FAILURE"} failure))

;; ## Dumb, predefined Actions

(def a:success (action {:name "SUCCESS"} success))

(def a:failure (action {:name "FAILURE"} failure))

(def a:running (action {:name "RUNNING"} running))

;; ## Some Decorator helpers

(defn d:success
  "Will try to get child executed and then return `Success` anyway.

  Will signal `Running` if child is `Running`."
  [node]
  (decorator {:name "SUCCESS"}
             (constantly true)
             (fn [deco stash]
               (let [flag (-> deco :children first :flag)]
                 (if (running? flag)
                   (running deco stash)
                   (success deco stash))))
             (constantly nil)
             node))

(defn d:failure
  "Will try to get child executed and then return `Failure` anyway.

  Will signal `Running` if child is `Running`."
  [node]
  (decorator {:name "FAILURE"}
             (constantly true)
             (fn [deco stash]
               (let [flag (-> deco :children first :flag)]
                 (if (running? flag)
                   (running deco stash)
                   (failure deco stash))))
             (constantly nil)
             node))

(defn d:inversion
  "Will try to get child executed and then reverse the `Status` if done.

  Will signal `Running` if shild is `Running`."
  [node]
  (decorator {:name "INVERSION"}
             (constantly true)
             (fn [deco stash]
               (let [flag (-> deco :children first :flag)]
                 (cond
                   (running? flag)
                   (running deco stash)
                   (success? flag)
                   (failure deco stash)
                   (failure? flag)
                   (success deco stash)
                   :else
                   (throw (ex-info "Invert WAT?" {:inverter deco
                                                  :stash stash})))))
             (constantly nil)
             node))

(defn d:shuffle
  "Will try to shuffle children of its child prior to every initial execution."
  [node]
  (decorator {:name "SHUFFLE"}
             (constantly false)
             (fn join-fn [deco stash]
               (let [flag (-> deco :children first :flag)]
                 (return-status flag deco stash)))
             (fn tick-fn [deco stash]
               (if (and (-> deco :children first :flag ready?)
                        (-> deco :children first (contains? :children))
                        (-> deco :children first :children count (> 1)))
                 (ready (-> deco
                            (assoc :branching? true)
                            (update-in [:children 0 :children]
                                       (comp vec shuffle)))
                        stash)
                 (ready (assoc deco :branching? true) stash)))
             node))

(defn d:immediate
  "If child is `Running`, rerun immediately via `Ready`."
  [node]
  (decorator {:name "IMMEDIATE"}
             (constantly true)
             (fn join-fn [deco stash]
               (let [flag (-> deco :children first :flag)]
                 (if (running? flag)
                   (ready deco stash)
                   (return-status flag deco stash))))
             (constantly nil)
             node))

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

