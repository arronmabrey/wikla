(ns wikla.core-test
  (:refer-clojure :exclude [sequence])
  (:require [wikla.core :refer :all]
            [midje.sweet :refer :all]))

(facts "Status"

  (let [s SUCCESS
        f FAILURE
        r RUNNING
        u READY]
    (fact "Properties and Payload of empty Status"
      (every? status? [s f r u]) => true?
      (mapv success? [s f r u]) => [true false false false]
      (mapv failure? [s f r u]) => [false true false false]
      (mapv running? [s f r u]) => [false false true false]
      (mapv ready? [s f r u]) => [false false false true]
      (mapv decided? [s f r u]) => [true true true false]
      (mapv done? [s f r u]) => [true true false false]
      (mapv has-node? [s f r u]) => [false false false false]
      (mapv has-stash? [s f r u]) => [false false false false]))

  (let [s (success (condition success) {:s true})
        f (failure (condition success) {:f true})
        r (running (condition success) {:r true})
        u (ready (condition success) {:u true})]

    (fact "Properties and Payload"
      (every? status? [s f r]) => true?
      (every? decided? [s f r]) => true?
      (every? has-node? [s f r]) => true?
      (every? has-stash? [s f r]) => true?)

    (fact "Success payload"
      s => success?
      (:node s) => node?
      (-> s :stash :s) => true?
      (-> s :node :flag) => success?)

    (fact "Failure payload"
      f => failure?
      (:node f) => node?
      (-> f :stash :f) => true?
      (-> f :node :flag) => failure?)

    (fact "Running payload."
      r => running?
      (:node r) => node?
      (-> r :stash :r) => true?
      (-> r :node :flag) => running?)

    (fact "Ready payload."
      u => ready?
      (:node u) => node?
      (-> u :stash :u) => true?
      (-> u :node :flag) => ready?)))

(facts "Condition"

  (fact "Construction and meta"
    (let [simpler (condition {:name "simple" :status-fn :x} failure)
          simple  (with-meta simpler
                             (merge (meta simpler) {:x :marks-the-spot}))]
      (reset simple) => simple
      (meta (reset simple)) => (meta simple)
      (:name simple) => "simple"
      (:status-fn simple) =not=> :x
      (condition {} constantly) => (condition constantly)))

  (fact "Given `status-fn` can only return SUCCESS or FAILURE"
    (tick (condition success) {}) => success?
    (tick (condition failure) {}) => failure?
    (tick (condition running) {}) => (throws Exception)
    (tick (condition ready) {}) => (throws Exception)
    (tick (condition (constantly :x))  {}) => (throws Exception))

  (facts "Exception in tick are not handled."
    (let [e (condition (fn [_ _] (throw (ex-info "Ooops!" {:oops true}))))]
      (tick e {}) => (throws Exception)
      (try (tick e {})
           (catch Exception e
             (ex-data e) => {:oops true}))))

  (fact "Changes made to condition and stash not persisted"
    (let [simple (condition {:original? true}
                            (fn [a-cond stash]
                              (success (assoc a-cond :changed? true)
                                       (assoc stash :changed? true))))
          ticked (tick simple {:original true})]
      (-> ticked :node :original?) => true?
      (-> ticked :node :changed?) => nil?
      (-> ticked :stash :original) => true?
      (-> ticked :stash :changed?) => nil?)))

(facts "Action"

  (fact "Construction and meta"
    (let [simpler (action {:name "simple" :tick-fn :x} failure)
          simple  (with-meta simpler
                             (merge (meta simpler) {:x :marks-the-spot}))]
      (reset simple) => simple
      (meta (reset simple)) => (meta simple)
      (:name simple) => "simple"
      (:tick-fn simple) =not=> :x
      (action {} success) => (action success)))

  (fact "Given `tick-fn` must return a Success, Failure or Running"
    (tick (action success) {}) => success?
    (tick (action failure) {}) => failure?
    (tick (action running) {}) => running?
    (tick (action ready) {}) => (throws Exception)
    (tick (action (constantly :x)) {}) => (throws Exception))

  (facts "Exception in tick are not handled."
    (let [e (action (fn [_ _] (throw (ex-info "Ooops!" {:oops true}))))]
      (tick e {}) => (throws Exception)
      (try (tick e {})
           (catch Exception e
             (ex-data e) => {:oops true}))))

  (fact "Action changes dropped, stash changes persisted"
    (let [simple (action {:original? true}
                         (fn [c s]
                           (success (assoc c :changed? true)
                                    (assoc s :changed? true))))
          ticked (tick simple {:original true})]
      (-> ticked :node :changed?) => nil?
      (-> ticked :node :original?) => true?
      (-> ticked :stash :changed?) => true?
      (-> ticked :stash :original) => true?)))

(facts "Sequence"

  (fact "Construction and meta"
    (let [borked  (action (constantly :x))
          simpler (sequence {:name "simple" :children :x} [borked])
          simple  (with-meta simpler
                             (merge (meta simpler) {:x :marks-the-spot}))]
      (reset simple) => simple
      (meta (reset simple)) => (meta simple)
      (:name simple) => "simple"
      (:children simple) =not=> :x
      (sequence {} []) => (throws AssertionError)
      (sequence {} [:not-a-node]) => (throws AssertionError)
      (sequence []) => (throws AssertionError)
      (sequence [:not-a-node]) => (throws AssertionError)
      (sequence {} [borked]) => (sequence [borked])))

  (fact "Parting"
    (let [borked (action (constantly :x))
          simple (sequence {:name "simple"} [borked])]
      (branch? simple) => true?
      (:flag simple) => ready?
      (-> simple part boolean) => true?
      (-> simple
          (join (tick (action success) {}))
          :node
          part
          boolean) => false?))

  (let [borked (action (constantly :x))
        mark  (fn [stash tag]
                (assoc stash :tag tag))
        s-act (-> (fn [a s] (success a (mark s :s)))
                  (action)
                  (tick {}))
        f-act (-> (fn [a s] (failure a (mark s :f)))
                  (action)
                  (tick {}))
        r-act (-> (fn [a s] (running a (mark s :r)))
                  (action)
                  (tick {}))
        s-one (sequence [borked])
        s-two (-> (sequence [borked borked])
                  (join s-act)
                  :node)]

    (fact "Single child join"
      (join s-one s-act) => success?
      (join s-one f-act) => failure?
      (join s-one r-act) => running?
      (-> s-one (join s-act) :stash :tag) => :s
      (-> s-one (join f-act) :stash :tag) => :f
      (-> s-one (join r-act) :stash :tag) => :r)

    (fact "Second child join"
      (:flag s-two) => ready?
      (join s-two s-act) => success?
      (join s-two f-act) => failure?
      (join s-two r-act) => running?
      (-> s-two (join s-act) :stash :tag) => :s
      (-> s-two (join f-act) :stash :tag) => :f
      (-> s-two (join r-act) :stash :tag) => :r)

    (fact "Running child re-join"
      (-> s-one (join r-act) :node (join s-act)) => success?
      (-> s-one (join r-act) :node (join f-act)) => failure?
      (-> s-one (join r-act) :node (join r-act)) => running?
      (-> s-two (join r-act) :node (join s-act)) => success?
      (-> s-two (join r-act) :node (join f-act)) => failure?
      (-> s-two (join r-act) :node (join r-act)) => running?)

    (fact "Failed joins"
      ; exceptions
      (join s-one :not-a-node) => (throws Exception)
      (-> s-one (join s-act) :node (join s-act)) => (throws Exception))))

(facts "Selection"

  (fact "Construction and meta"
    (let [borked  (action (constantly :x))
          simpler (selection {:name "simple" :children :x} [borked])
          simple  (with-meta simpler
                             (merge (meta simpler) {:x :marks-the-spot}))]
      (reset simple) => simple
      (meta (reset simple)) => (meta simple)
      (:name simple) => "simple"
      (:children simple) =not=> :x
      (selection {} []) => (throws AssertionError)
      (selection {} [:not-a-node]) => (throws AssertionError)
      (selection []) => (throws AssertionError)
      (selection [:not-a-node]) => (throws AssertionError)
      (selection {} [borked]) => (selection [borked])))

  (let [borked (action (constantly :x))
        mark  (fn [stash tag]
                (assoc stash :tag tag))
        s-act (-> (fn [a s] (success a (mark s :s)))
                  (action)
                  (tick {}))
        f-act (-> (fn [a s] (failure a (mark s :f)))
                  (action)
                  (tick {}))
        r-act (-> (fn [a s] (running a (mark s :r)))
                  (action)
                  (tick {}))
        s-one (selection [borked])
        s-two (-> (selection [borked borked])
                  (join f-act)
                  :node)]

    (fact "Single child join"
      (join s-one s-act) => success?
      (join s-one f-act) => failure?
      (join s-one r-act) => running?
      (-> s-one (join s-act) :stash :tag) => :s
      (-> s-one (join f-act) :stash :tag) => :f
      (-> s-one (join r-act) :stash :tag) => :r)

    (fact "Second child join"
      (:flag s-two) => ready?
      (join s-two s-act) => success?
      (join s-two f-act) => failure?
      (join s-two r-act) => running?
      (-> s-two (join s-act) :stash :tag) => :s
      (-> s-two (join f-act) :stash :tag) => :f
      (-> s-two (join r-act) :stash :tag) => :r)

    (fact "Running child re-join"
      (-> s-one (join r-act) :node (join s-act)) => success?
      (-> s-one (join r-act) :node (join f-act)) => failure?
      (-> s-one (join r-act) :node (join r-act)) => running?
      (-> s-two (join r-act) :node (join s-act)) => success?
      (-> s-two (join r-act) :node (join f-act)) => failure?
      (-> s-two (join r-act) :node (join r-act)) => running?)

    (fact "Failed joins"
      ; exceptions
      (join s-one :not-a-node) => (throws Exception)
      (-> s-one (join s-act) :node (join s-act)) => (throws Exception))))

(facts "Parallel"

  (fact "Construction and meta"
    (let [borked  (action (constantly :x))
          simpler (parallel {:name "simple" :children :x} 1 1 [borked])
          simple  (with-meta simpler
                             (merge (meta simpler) {:x :marks-the-spot}))]
      (reset simple) => simple
      (meta (reset simple)) => (meta simple)
      (:name simple) => "simple"
      (:children simple) =not=> :x
      (parallel {} 2 2 [borked]) => (throws AssertionError)
      (parallel {} 0 1 [borked]) => (throws AssertionError)
      (parallel {} :0 :1 [borked]) => (throws AssertionError)
      (parallel {} 2 2 []) => (throws AssertionError)
      (parallel {} 0 1 []) => (throws AssertionError)
      (parallel {}  :2 :2 []) => (throws AssertionError)
      (parallel {}  1 1 [1]) => (throws AssertionError)  
      (parallel 2 2 [borked]) => (throws AssertionError)
      (parallel 0 1 [borked]) => (throws AssertionError)
      (parallel :0 :1 [borked]) => (throws AssertionError)
      (parallel 2 2 []) => (throws AssertionError)
      (parallel 0 1 []) => (throws AssertionError)
      (parallel :2 :2 []) => (throws AssertionError)
      (parallel 1 1 [1]) => (throws AssertionError)
      (parallel {} 1 1 [borked]) => (parallel 1 1 [borked])))

  (fact "Basic interface."
    (let [borked (action (constantly :x))
          simple (parallel {:name "simple"} 1 1 [borked])]
      (branch? simple) => true?
      (:flag simple) => ready?
      (-> simple part boolean) => true?
      (-> simple
          (join (tick (action success) {}))
          :node
          part
          boolean) => false?))

  (let [borked (action (constantly :x))
        mark   (fn [stash tag]
                 (assoc stash :tag tag))
        s-act  (-> (fn [a s] (success a (mark s :s)))
                   (action)
                   (tick {}))
        f-act  (-> (fn [a s] (failure a (mark s :f)))
                   (action)
                   (tick {}))
        r-act  (-> (fn [a s] (running a (mark s :r)))
                   (action)
                   (tick {}))
        p-one  (parallel 1 1 [borked])
        p-succ (-> (parallel 2 2 [borked borked])
                  (join s-act)
                  :node)
        p-fail (-> (parallel 2 2 [borked borked])
                  (join f-act)
                  :node)]

    (fact "Single child join"
      (join p-one s-act) => success?
      (join p-one f-act) => failure?
      (join p-one r-act) => running?
      (-> p-one (join s-act) :stash :tag) => :s
      (-> p-one (join f-act) :stash :tag) => :f
      (-> p-one (join r-act) :stash :tag) => :r)

    (fact "Second child join"
      (:flag p-succ) => ready?
      (join p-succ s-act) => success?
      (join p-succ f-act) => running?
      (join p-succ r-act) => running?
      (-> p-succ (join s-act) :stash :tag) => :s
      (-> p-succ (join f-act) :stash :tag) => :f
      (-> p-succ (join r-act) :stash :tag) => :r
      (:flag p-fail) => ready?
      (join p-fail s-act) => running?
      (join p-fail f-act) => failure?
      (join p-fail r-act) => running?
      (-> p-fail (join s-act) :stash :tag) => :s
      (-> p-fail (join f-act) :stash :tag) => :f
      (-> p-fail (join r-act) :stash :tag) => :r)

    (fact "Running child re-join"
      (-> p-one (join r-act) :node (join s-act)) => success?
      (-> p-one (join r-act) :node (join f-act)) => failure?
      (-> p-one (join r-act) :node (join r-act)) => running?
      (-> p-succ (join r-act) :node (join s-act)) => success?
      (-> p-succ (join r-act) :node (join f-act)) => running?
      (-> p-succ (join r-act) :node (join r-act)) => running?
      (-> p-fail (join r-act) :node (join s-act)) => running?
      (-> p-fail (join r-act) :node (join f-act)) => failure?
      (-> p-fail (join r-act) :node (join r-act)) => running?)

    (fact "Failed joins"
      ; exceptions
      (join p-one :not-a-node) => (throws Exception)
      (-> p-one (join s-act) :node (join s-act)) => (throws Exception))))

(facts "Decorators"

  (fact "Construction and meta"
    (let [simpler (decorator {:name "simple" :children :x}
                             (constantly true)
                             success
                             failure
                             (action success))
          simple  (with-meta simpler
                             (merge (meta simpler) {:x :marks-the-spot}))
          bork    (action success)
          tru     (constantly true)]
      (reset simple) => simple
      (meta (reset simple)) => (meta simple)
      (:name simple) => "simple"
      (:children simple) =not=> :x
      (decorator {}
                 (constantly true)
                 success
                 failure
                 :not-a-node) => (throws AssertionError)
      (decorator (constantly true)
                 success
                 failure
                 :not-a-node) => (throws AssertionError)
      (decorator {} tru success failure bork)
        => (decorator tru success failure bork)))

  (fact "Tick and join"
    (let [mark   (fn [stash tag]
                   (assoc stash :tag tag))
          s-act  (action (fn [a s] (success a (mark s :s))))
          f-act  (action (fn [a s] (failure a (mark s :s))))
          r-act  (action (fn [a s] (running a (mark s :s))))
          simple (decorator {:name "simple brancher"}
                            (constantly true)
                            (fn [d s]
                              (if (-> d :children first :flag running?)
                                (running d s)
                                (success d s)))
                            failure
                            s-act)
          ticked (tick simple {})]
      (branch? simple) => false?
      ticked => ready?
      (-> ticked :node branch?) => true?
      (join (:node ticked) (tick s-act {})) => success?
      (join (:node ticked) (tick f-act {})) => success?
      (join (:node ticked) (tick r-act {})) => running?
      (-> (tick simple {:original true})
          :stash
          :original) => true?
      (-> (tick simple {})
          :node
          (join (tick s-act {:original true}))
          :stash
          :original) => true?
      (-> (tick simple {})
          :node
          (join (tick s-act {:original true}))
          :stash
          :tag) => :s
      (-> (tick simple {})
          :node
          (tick {})) => (throws Exception)))

  (fact "Just tick"
    (let [simple (decorator {:name "simple ticker"}
                            (constantly false)
                            (fn [d _ s]
                              (success d s))
                            failure
                            (action success))]
      (branch? simple) => false?
      (part simple) => nil?
      (tick simple {}) => failure?
      ; keeping stash intact
      (-> simple
          (tick {:original true})
          :stash
          :original) => true?
      ; exceptions
      (join simple (tick (action success) {})) => (throws Exception))))

(facts "Tickers"

  (fact "Construction and meta"
    (let [borked  (action (constantly :x))
          simple0 (ticker {:name "simple" :tree :x} borked)
          simple  (with-meta simple0
                             (merge (meta simple0) {:x :marks-the-spot}))]
      (reset simple) => simple
      (meta (reset simple)) => (meta simple)
      (:name simple) => "simple"
      (:tree simple) =not=> :x
      (ticker {} :not-a-node) => (throws AssertionError)
      (ticker :not-a-node) => (throws AssertionError)
      (ticker {} borked) => (ticker borked)))

  (facts "Exception Exception catcher."
    (let [error  (action (fn [_ _] (throw (ex-info "Ooops!" {:oops true}))))
          stash  {:original true}
          simple (ticker {} error)]
      (tick simple stash) => (throws Exception)
      (try
        (tick simple stash)
        (catch Exception e
          (ex-data e) => {:stash stash :ticker simple :node error}))))

  (fact "Single node tick"
    (let [mark   (fn [stash tag]
                   (assoc stash :tag tag))
          s-act  (action (fn [a s] (success a (mark s :s))))
          f-act  (action (fn [a s] (failure a (mark s :s))))
          r-act  (action (fn [a s] (running a (mark s :s))))
          simple (ticker {} s-act)
          stash  {:original :ticker}
          ticked (tick simple stash)]
      ticked => success?
      (-> ticked :stash :original) => :ticker
      (-> ticked :stash :tag) => :s
      ; variety
      (tick (ticker f-act) {}) => failure?
      (tick (ticker r-act) {}) => running?
      ; re-running
      (tick (:node ticked) (:stash ticked)) => (throws Exception)
      (tick (reset (:node ticked)) (:stash ticked)) => success?))

  (fact "Deep tick"
    (let [f-act    (action (fn [a s]
                             (failure a
                                      (update-in s
                                                 [:trace]
                                                 (fnil conj [])
                                                 :f))))
          deep-f   (ticker
                     (sequence
                       [(selection
                          [(parallel 1 1 [f-act])])]))
          ticked-f (tick deep-f {:original :ticker})]
      ticked-f => failure?
      (-> ticked-f :stash :original) => :ticker
      (-> ticked-f :stash :trace) => [:f]
      ; re-running
      (tick (:node ticked-f) (:stash ticked-f)) => (throws Exception)
      (tick (reset (:node ticked-f)) (:stash ticked-f)) => failure?
      (-> ticked-f
          :node
          (reset)
          (tick (:stash ticked-f))
          :stash
          :trace) => [:f :f])))

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

