(ns wikla.dot-test
  (:refer-clojure :exclude [sequence])
  (:require [wikla.core :as wi :refer :all]
            [wikla.ext :as ext :refer [robin]]
            [wikla.pre :as pre :refer [c:success a:success d:inversion]]
            [wikla.dot :as dot]
            [midje.sweet :refer :all]))

(facts "DOT"

  (let [a-sequence  (sequence {:name "S"} [c:success])
        a-selection (selection {:name "S"} [c:success])
        a-parallel  (parallel {:name "P"} 1 1 [c:success])
        a-decorator (d:inversion c:success)
        a-ticker    (ticker {:name "T"} c:success)
        a-robin     (robin {:name "R"} success [c:success])
        all-known   [c:success a:success a-sequence a-selection a-parallel
                      a-decorator a-ticker a-robin]]

    (fact "Protocol definitions"
      (every? (partial satisfies? dot/Dot) all-known) => true?)

    (fact "DOT on Condition"
      (dot/shape c:success) => "oval"
      (dot/label (dissoc c:success :name)) => "Condition"
      (dot/label c:success) => "Condition: SUCCESS"
      (dot/children c:success) => nil?
      (dot/color c:success) => "silver")

    (fact "DOT on Action"
      (dot/shape a:success) => "rect"
      (dot/label (dissoc a:success :name)) => "Action"
      (dot/label a:success) => "Action: SUCCESS"
      (dot/children a:success) => nil?
      (dot/color a:success) => "silver")

    (fact "DOT on Sequence")
      (dot/shape a-sequence) => "rect"
      (dot/label (dissoc a-sequence :name)) => "Sequence"
      (dot/label a-sequence) => "Sequence: S"
      (dot/children a-sequence) => [c:success]
      (dot/color a-sequence) => "silver"

    (fact "DOT on Selection")
      (dot/shape a-selection) => "rect"
      (dot/label (dissoc a-selection :name)) => "Selection"
      (dot/label a-selection) => "Selection: S"
      (dot/children a-selection) => [c:success]
      (dot/color a-selection) => "silver"

    (fact "DOT on Parallel")
      (dot/shape a-parallel) => "rect"
      (dot/label (dissoc a-parallel :name)) => "Parallel (s=1, f=1)"
      (dot/label a-parallel) => "Parallel (s=1, f=1): P"
      (dot/children a-parallel) => [c:success]
      (dot/color a-parallel) => "silver"

    (fact "DOT on Decorator")
      (dot/shape a-decorator) => "rect"
      (dot/label (dissoc a-decorator :name)) => "Decorator"
      (dot/label a-decorator) => "Decorator: INVERSION"
      (dot/children a-decorator) => [c:success]
      (dot/color a-decorator) => "silver"

    (fact "DOT on Ticker")
      (dot/shape a-ticker) => "diamond"
      (dot/label (dissoc a-ticker :name)) => "Ticker"
      (dot/label a-ticker) => "Ticker: T"
      (dot/children a-ticker) => [c:success]
      (dot/color a-ticker) => "silver"

    (fact "DOT on Robin")
      (dot/shape a-robin) => "rect"
      (dot/label (dissoc a-robin :name)) => "Robin"
      (dot/label a-robin) => "Robin: R"
      (dot/children a-robin) => [c:success]
      (dot/color a-robin) => "silver")

    (fact "DOT on things with Status"
      (let [s (:node (success c:success {}))
            f (:node (failure c:success {}))
            r (:node (running c:success {}))]
        (dot/color s) => "lime"
        (dot/color f) => "crimson"
        (dot/color r) => "deepskyblue")))

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai


