(ns wikla.ext-test
  (:refer-clojure :exclude [sequence])
  (:require [wikla.core :as wi :refer :all]
            [wikla.ext :as ext :refer :all]
            [wikla.pre :as pre :refer :all]
            [midje.sweet :refer :all]))

(facts "Robin"

  (fact "Construction and meta"
    (let [borked  (action (constantly :x))
          simpler (robin {:name "simple"} success [borked borked borked])
          simple  (with-meta simpler
                             (merge (meta simpler) {:x :marks-the-spot}))]
      (reset simple) => simple
      (meta (reset simple)) => (meta simple)
      (:name simple) => "simple"
      (:children simple) =not=> :x
      (robin {} success []) => (throws AssertionError)
      (robin {} success [:not-a-node]) => (throws AssertionError)
      (robin success []) => (throws AssertionError)
      (robin success [:not-a-node]) => (throws AssertionError)
      (robin {} success [borked]) => (robin success [borked])))

  (let [borked (action (constantly :x))
        mark  (fn [stash tag]
                (assoc stash :tag tag))
        s-act (-> (fn [a s] (success a (mark s :s)))
                  (action)
                  (tick {}))
        f-act (-> (fn [a s] (failure a (mark s :f)))
                  (action)
                  (tick {}))
        r-act (-> (fn [a s] (running a (mark s :r)))
                  (action)
                  (tick {}))
        s-one (robin success [borked])
        s-two (-> (robin success [borked borked])
                  (join r-act)
                  :node)]

    (fact "Single child join"
      (join s-one s-act) => success?
      (join s-one f-act) => success?
      (join s-one r-act) => ready?
      (-> s-one (join s-act) :stash :tag) => :s
      (-> s-one (join f-act) :stash :tag) => :f
      (-> s-one (join r-act) :stash :tag) => :r)

    (fact "Second child join"
      (:flag s-two) => ready?
      (join s-two s-act) => running?
      (join s-two f-act) => running?
      (join s-two r-act) => running?
      (-> s-two (join s-act) :stash :tag) => :s
      (-> s-two (join f-act) :stash :tag) => :f
      (-> s-two (join r-act) :stash :tag) => :r)

    (fact "Running child re-join"
      (-> s-one (join r-act) :node (join s-act)) => success?
      (-> s-one (join r-act) :node (join f-act)) => success?
      (-> s-one (join r-act) :node (join r-act)) => running?
      (-> s-two (join r-act) :node (join s-act)) => running?
      (-> s-two (join r-act) :node (join f-act)) => running?
      (-> s-two (join r-act) :node (join r-act)) => running?
      (-> s-one (join r-act) :node (join r-act) :node (join f-act)) => success?
      (-> s-two (join r-act) :node (join s-act) :node (join s-act)) => success?
      (-> s-two (join r-act) :node (join r-act) :node (join r-act)) => running?)

    (fact "Failed joins"
      ; exceptions
      (join s-one :not-a-node) => (throws Exception)
      (-> s-one (join s-act) :node (join s-act)) => (throws Exception))))

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

