(ns wikla.pre-test
  (:refer-clojure :exclude [sequence])
  (:require [wikla [core :as wi :refer :all]
                   [pre :as pre :refer :all]]
            [midje.sweet :refer :all]))

(facts "Pre-set Conditions"
  (let [s c:success
        f c:failure]
    ;(every? ready? (map :flag [s f])) => true?
    (tick s {}) => success?
    (tick f {}) => failure?
    (-> s (tick {:s true}) :stash) => {:s true}
    (-> f (tick {:f true}) :stash) => {:f true}))

(facts "Pre-set Actions"
  (let [s a:success
        f a:failure
        r a:running]
    ;(every? ready? (map :flag [s f r])) => true?
    (tick s {}) => success?
    (tick f {}) => failure?
    (tick r {}) => running?
    (-> s (tick {:s true}) :stash) => {:s true}
    (-> f (tick {:f true}) :stash) => {:f true}
    (-> r (tick {:r true}) :stash) => {:r true}))

(facts "Pre-set Decorators"

  (fact "Success Decorator"
    (let [d-succ (d:success a:failure)
          ticked (:node (tick d-succ {}))]
      (:flag d-succ) => ready?
      (branch? d-succ) => false?
      (:flag ticked) => ready?
      (branch? ticked) => true?
      (join ticked (success a:success {})) => success?
      (join ticked (failure a:failure {})) => success?
      (join ticked (running a:running {})) => running?
      (join ticked (ready a:running {}))   => (throws Exception)))

  (fact "Failure Decorator"
    (let [d-fail (d:failure a:success)
          ticked (:node (tick d-fail {}))]
      (:flag d-fail) => ready?
      (branch? d-fail) => false?
      (:flag ticked) => ready?
      (branch? ticked) => true?
      (join ticked (success a:success {})) => failure?
      (join ticked (failure a:failure {})) => failure?
      (join ticked (running a:running {})) => running?
      (join ticked (ready a:running {}))   => (throws Exception)))

  (fact "Inversion Decorator"
    (let [d-invr (d:inversion a:running)
          ticked (:node (tick d-invr {}))]
      (:flag d-invr) => ready?
      (branch? d-invr) => false?
      (:flag ticked) => ready?
      (branch? ticked) => true?
      (join ticked (success a:success {})) => failure?
      (join ticked (failure a:failure {})) => success?
      (join ticked (running a:running {})) => running?
      (join ticked (ready a:running {}))   => (throws Exception)))

  (fact "Shuffle Decorator"
    (let [tickr  (fn [d] (-> d (tick {}) :children first :children))
          a-seq  (sequence [c:success c:failure a:success a:failure])
          d-shuf (d:shuffle a-seq)
          ticked (:node (tick d-shuf {}))]
      (:flag d-shuf) => ready?
      (branch? d-shuf) => false?
      (:flag ticked) => ready?
      (branch? ticked) => true?
      (:children a-seq) => (-> d-shuf :children first :children)
      (some (partial not= (:children a-seq))
            (map tickr (repeat 99 d-shuf))) => true?))

  (fact "Immediate Decorator"
    (let [borked (action (constantly :x))
          d-imme (d:immediate borked)
          ticked (:node (tick d-imme {}))]
      (:flag d-imme) => ready?
      (branch? d-imme) => false?
      (:flag ticked) => ready?
      (branch? ticked) => true?
      (join ticked (success a:success {})) => success?
      (join ticked (failure a:success {})) => failure?
      (join ticked (running a:success {})) => ready?)))

; vim:tw=80 cc=+1 co=114 ts=2 sw=2 et ai

